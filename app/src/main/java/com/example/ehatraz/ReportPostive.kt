package com.example.ehatraz

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.ehatraz.firebase.FirebaseManger
import com.example.ehatraz.storage.StorgeManger
import kotlinx.android.synthetic.main.activity_log.*
import kotlinx.android.synthetic.main.activity_log.submitButton
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_report_postive.*
import kotlinx.android.synthetic.main.login_dailog.*
import kotlinx.android.synthetic.main.login_dailog.view.*
import java.text.SimpleDateFormat
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ReportPostive.newInstance] factory method to
 * create an instance of this fragment.
 */
class ReportPostive : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button.setOnClickListener{
            val mDailog = LayoutInflater.from(context).inflate(R.layout.login_dailog,null)
            val mBulider = AlertDialog.Builder(context).setView(mDailog).setTitle("Login")
            val mAler = mBulider.show();
            mDailog.login.setOnClickListener{
                //mAler.dismiss();
                val username = mDailog.username.text.toString()
                val password = mDailog.password.text.toString();
                val firebase = FirebaseManger.getInstance(context?.applicationContext)

            }
        }
        quarntineBtn.setOnClickListener{
            val mDailog = LayoutInflater.from(context).inflate(R.layout.geofence_dailog,null)
            val mBulider = AlertDialog.Builder(context).setView(mDailog).setTitle("Qurantine")
            val mAler = mBulider.show();
            mDailog.login.setOnClickListener{
                //mAler.dismiss();
                val username = mDailog.username.text.toString()
                val password = mDailog.password.text.toString();
                val firebase = FirebaseManger.getInstance(context?.applicationContext)

            }
        }


    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_report_postive, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ReportPostive.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ReportPostive().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

}